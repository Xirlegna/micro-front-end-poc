import AppHeader from './components/AppHeader/AppHeader';

const App: React.FC = () => {
  return (
    <div>
      <h4>components</h4>
      <AppHeader />
    </div>
  );
};

export default App;
